Template: sympa/language
Type: select
Choices: ${supported_langs}
DefaultChoice: en
_Description: Default language for Sympa:

Template: sympa/hostname
Type: string
_Description: Sympa hostname:
 This is the name of the machine or the alias you will use to reach sympa.
 .
 Example:
 .
   listhost.cru.fr
 .
   Then, you will send your sympa commands to:
 .
   sympa@listhost.cru.fr

Template: sympa/listmaster
Type: string
_Description: Listmaster email address(es):
 Listmasters are privileged people who administrate mailing lists (mailing
 list superusers).
 .
 Please give listmasters email addresses separated by commas.
 .
 Example:
 .
   postmaster@cru.fr, root@home.cru.fr

Template: sympa/remove_spool
Type: boolean
Default: false
_Description: Should lists home and spool directories be removed?
 The lists home directory (/var/lib/sympa) contains the mailing lists
 configurations, mailing list archives and S/MIME user certificates
 (when sympa is configured for using S/MIME encryption and authentication).
 The spool directory (/var/spool/sympa) contains various queue directories.
 .
 Note that if those directories are empty, they will be automatically
 removed.
 .
 Please choose whether you want to remove lists home and spool directories.

Template: wwsympa/wwsympa_url
Type: string
_Description: URL to access WWSympa:

Template: wwsympa/webserver_type
Type: select
__Choices: Apache 2, Other
Default: Apache 2
_Description: Which Web Server(s) are you running?

Template: sympa/use_soap
Type: boolean
Default: false
_Description: Do you want the sympa SOAP server to be used?
 Sympa SOAP server allows to access a Sympa service from within another
 program, written in any programming language and on any computer. The SOAP
 server provides a limited set of high level functions including login,
 which, lists, subscribe, signoff.
 .
 The SOAP server uses libsoap-lite-perl package and a webserver like apache.

Template: wwsympa/remove_spool
Type: boolean
Default: false
_Description: Should the web archives and the bounce directory be removed?
 If you used the default configuration, WWSympa web archives are located in
 /var/lib/sympa/wwsarchive. The WWSympa bounce directory contains bounces
 (non-delivery reports) and is set to /var/spool/sympa/wwsbounce by
 default.
 .
 Please choose whether you want to remove the web archives and the bounce
 directory.

Template: sympa/sympa_newaliases-wrapper-setuid-root
Type: boolean
Default: false
_Description: Should sympa_newaliases-wrapper be setuid root?
 Program 'sympa_newaliases-wrapper' is run with root privileges to
 allow sympa to update email aliases when creating or deleting lists,
 but this can lead to security issues (see CVE-2020-26880).
 .
 In most email environments (exim, postfix) sympa doesn't need root
 privileges (see also the 'aliases_program' parameter in sympa.conf).
 .
 Please choose whether you want to install this program with the
 setuid bit set (u+s).
